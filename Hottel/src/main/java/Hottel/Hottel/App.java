package Hottel.Hottel;


import java.util.Scanner;

import billsAndReservationPackage.BillsList;
import billsAndReservationPackage.ReservationList;
import clientsPackage.ClientsList;
import roomPackage.RoomList;

public class App {
    public static void main( String[] args ){

    	ClientsList cl = new ClientsList();
    	RoomList rl = new RoomList();

    	rl.initializeRoomList();
    	
    	cl.initializeClientsList();
    	
    	ReservationList reservationList = new ReservationList();
    	reservationList.createReservationList(cl.getClientsList(),rl);
    	
    	System.out.println("\nDo you want to make a new reservation?(1-yes,0-no)");
    	Scanner input = new Scanner(System.in);
    	int addClients=input.nextInt();
    	while(addClients!=0){
    		reservationList.addReservationToList(cl.getClientsList(), rl);
    		System.out.println("\nDo you want to make a new reservation?(1-yes,0-no)");
    		addClients=input.nextInt();
    	}
    	input.close();
    	
    	System.out.println("Bills: ");
    	BillsList bills = new BillsList();
    	bills.createBillsList(reservationList.getReservationsList());
    	bills.showAllBills();
    }
}
 
  