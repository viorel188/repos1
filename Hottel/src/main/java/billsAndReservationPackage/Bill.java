package billsAndReservationPackage;


import java.util.Date;

import clientsPackage.Client;
import roomPackage.Room;


public class Bill {
	private final double SALE=0.15;
	private Client cl;
	private Room rm;
	private Date checkInDate;
	private Date checkOutDate;
	private double totalToPay;
	
	public Bill(Client c, Room r, Date checkInDate, Date checkOutDate){
		cl = c;
		rm = r;
		this.checkInDate=checkInDate;
		this.checkOutDate=checkOutDate;
	}
	
	public int totalDays(){
		long MILLIS_PER_DAY = 24 * 3600 * 1000;
		long msDiff= checkOutDate.getTime() - checkInDate.getTime();
		int daysDiff = (int)(Math.round(msDiff / ((double)MILLIS_PER_DAY)));
		return daysDiff;
	}
	
	public void totalCost(){
		totalToPay = totalDays()*rm.getPrice()-cl.getFidelity()*SALE;
	}
	
	public double getTotalAmountToBePaid(){
		return totalToPay;
	}

	public Client getCl() {
		return cl;
	}

	public Room getRm() {
		return rm;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}
}
