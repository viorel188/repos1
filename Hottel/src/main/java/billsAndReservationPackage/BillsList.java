package billsAndReservationPackage;

import java.util.LinkedList;

public class BillsList {
	private LinkedList<Bill> billsList;

	public void createBillsList(LinkedList<Reservation> reservationsList){
		billsList = new LinkedList<Bill>();
		for(Reservation res : reservationsList){
			billsList.add(new Bill(res.getCl(), res.getR(), res.getPrefferedCheckInDate(), res.getPrefferedCheckOutDate()));
		}
	}
	
	public void showAllBills(){
		for(Bill b : billsList){
			b.totalCost();
			System.out.println("\nClient: " + b.getCl().getCNP() + " has to pay: " + b.getTotalAmountToBePaid());
			System.out.println("for his " + b.totalDays() + " days accomodation at the hotel during "
					+ "the period: " + b.getCheckInDate() + " and " + b.getCheckOutDate());
			System.out.println("in roomNr " + b.getRm().getRoomNumber() + " for " + b.getRm().getPrice() + "$/night" );
		}
	}
	
	public LinkedList<Bill> getBillsList() {
		return billsList;
	}

	public void setBillsList(LinkedList<Bill> billsList) {
		this.billsList = billsList;
	}
	
	
}
