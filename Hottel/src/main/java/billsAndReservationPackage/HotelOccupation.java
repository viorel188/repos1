package billsAndReservationPackage;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import roomPackage.Room;

public class HotelOccupation {

	private LinkedList<OccupationPerRoom> occupationList;		// for each room's occupation intervals

	public HashMap<Date,Date> getOccupationForRoom(int roomNumber){
		for(OccupationPerRoom dates : occupationList){
			if(dates.getRoomNumber() == roomNumber){
				return dates.getOccupationDates();
			}
		}
		return new HashMap<Date,Date>();
	}
	//**
	public void setOccupationForRoom(int roomNumber, Date checkIn, Date checkOut){
		for(OccupationPerRoom dates : occupationList){
			if(dates.getRoomNumber() == roomNumber){
				dates.addDateInterval(checkIn, checkOut);
			}
		}
	}
	//////////////////
	public void initializeOccupationList(LinkedList<Room> roomList){
		occupationList = new LinkedList<OccupationPerRoom>();
		for(Room r : roomList){
			OccupationPerRoom opr = new OccupationPerRoom(r);
			occupationList.add(opr);
		}
	}
	////////////////////
	public void addElement(OccupationPerRoom opr){
		occupationList.add(opr);
	}

	public LinkedList<OccupationPerRoom> getOccupationList() {
		return occupationList;
	}
}
