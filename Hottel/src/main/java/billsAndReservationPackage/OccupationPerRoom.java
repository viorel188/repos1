package billsAndReservationPackage;

import java.util.Date;
import java.util.HashMap;

import roomPackage.Room;

public class OccupationPerRoom {
	public OccupationPerRoom(Room r) {
		this.r = r;
	}

	private Room r;
	private HashMap<Date,Date> occupationDates = new HashMap<Date,Date>();
	
	public void addDateInterval(Date beg, Date end){
		occupationDates.put(beg,end);
	}
	
	public int getRoomNumber(){
		return r.getRoomNumber();
	}
	
	public Room getRoom(){
		return r;
	}
	
	public HashMap<Date,Date> getOccupationDates(){
		return occupationDates;
	}
}
