package billsAndReservationPackage;

import java.util.Date;
import java.util.LinkedList;

import utilitary.FileRead;

public class OccupationRequest {
	public OccupationRequest(String cNP, int nrBeds, Date beg, Date end) {
		CNP = cNP;
		this.nrBeds = nrBeds;
		this.beg = beg;
		this.end = end;
	}
	private String CNP;
	private int nrBeds;
	private Date beg;
	private Date end;
	
	public static LinkedList<OccupationRequest> initializeOccupationList(){
		FileRead rf = new FileRead();
		rf.readingOccupation();
		return rf.getOccupationList();
	}
//getters
	public String getCNP() {
		return CNP;
	}
	public int getNrBeds() {
		return nrBeds;
	}
	public Date getBeg() {
		return beg;
	}
	public Date getEnd() {
		return end;
	}
}
