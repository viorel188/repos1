package billsAndReservationPackage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import clientsPackage.Client;
import roomPackage.Room;
import roomPackage.RoomList;
import utilitary.DateTimeUtil;

public class Reservation {
	private Room r;
	private Client cl;
	private Date prefferedCheckInDate;		//interval in which the client wishes to live in hotel
	private Date prefferedCheckOutDate;		//			
//////////////////////////////	
	public Reservation(Room r, Client cl, Date checkInDate, Date checkOutDate) {
		this.r = r;
		this.cl = cl;
		this.prefferedCheckInDate = checkInDate;
		this.prefferedCheckOutDate = checkOutDate;
	}
	
	public static boolean canBeReserved(HotelOccupation ho, Room roomID, Date prefBegin, 
									Date prefEnd){  // can be reserved "Room r" during the preffered period ? 
		DateTimeUtil dtu = new DateTimeUtil();
		HashMap<Date,Date> occupationDates = ho.getOccupationForRoom(roomID.getRoomNumber());
		if( occupationDates.size() == 0 ){
			//
			ho.setOccupationForRoom(roomID.getRoomNumber(), prefBegin, prefEnd);
			return true;
		}else{
			for( Date beg : occupationDates.keySet() ){
				Date end = occupationDates.get(beg);
				boolean overlap;
				overlap = dtu.intervalOverlap(prefBegin,prefEnd, beg,end);
				if( overlap ){
					return false;
				}
			}
		}
		// we add interval to the occupationMap of the room; so that next time we could compare with the last reservations made for room X
		ho.setOccupationForRoom(roomID.getRoomNumber(), prefBegin, prefEnd);
		return true;
	}
	
	public static Room availableRoomFromList(HotelOccupation ho, LinkedList<Room> roomIds, 
			Date prefBegin, Date prefEnd){
		for( Room roomID : roomIds){
			if( canBeReserved(ho, roomID,prefBegin,prefEnd) ){
				return roomID;
			}
		}
		return null;
	}
	
	public static Room availableRoomWithNrOfBeds(HotelOccupation ho, int nrBeds, RoomList rList, 
			Date prefBegin, Date prefEnd){
		LinkedList<Room> roomIds = rList.getRoomsWithNrOfBeds(nrBeds);
		return availableRoomFromList(ho,roomIds,prefBegin,prefEnd);
	}
	
	public static Room availableRoomWithSpecialConditions(HotelOccupation ho, Integer[] s, RoomList rList,  
		Date prefBegin, Date prefEnd)	//s = {floor,nrBeds,balcony,TV,miniBar,Internet,kitchen,conditioning}
	{
		LinkedList<Room> roomsWithDemandedConditions = rList.getRoomsWithSpecialConditions(s);
		Room room = availableRoomFromList(ho,roomsWithDemandedConditions,prefBegin,prefEnd);
		return room;
	}
	
	public void showForWhomWasMadeReservation(){
		System.out.println("\nReservation made for ");
		System.out.println("Client with the CNP: " + cl.getCNP());
		System.out.println("in RoomNr: " + r.getRoomNumber());
		System.out.println("During the period:");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		System.out.println(sdf.format(this.prefferedCheckInDate) + " and " + sdf.format(this.prefferedCheckOutDate));

	}
	// ****************************************

//gets&sets
	public Room getR() {
		return r;
	}
	public void setR(Room r) {
		this.r = r;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	public Date getPrefferedCheckInDate() {
		return prefferedCheckInDate;
	}
	public void setPrefferedCheckInDate(Date checkInDate) {
		this.prefferedCheckInDate = checkInDate;
	}
	public Date getPrefferedCheckOutDate() {
		return prefferedCheckOutDate;
	}
	public void setPrefferedCheckOutDate(Date checkOutDate) {
		this.prefferedCheckOutDate = checkOutDate;
	}
}
