package billsAndReservationPackage;


import java.text.SimpleDateFormat;
import java.util.LinkedList;

import clientsPackage.Client;
import clientsPackage.ClientsList;
import roomPackage.Room;
import roomPackage.RoomList;
import utilitary.AccomodationInterval;
import utilitary.DateTimeUtil;

public class ReservationList {
	private LinkedList<Reservation> reservationsList = new LinkedList<Reservation>();
	private HotelOccupation ho = new HotelOccupation();
	
	public void createReservationList(LinkedList<Client> clientsList, RoomList roomsList){
		ho.initializeOccupationList(roomsList.getRoomsList());
		LinkedList<OccupationRequest> occupationList = OccupationRequest.initializeOccupationList();
		for( OccupationRequest occup : occupationList){
			Client c = null;
			for( Client client : clientsList){
				if( occup.getCNP().equals(client.getCNP()) ){
					c=client;
					break;
				}
			}
			Room r = Reservation.availableRoomWithNrOfBeds(ho, occup.getNrBeds(), roomsList, 
																		occup.getBeg(), occup.getEnd());
			if(r!=null){
				Reservation res = new Reservation(r,c,occup.getBeg(),occup.getEnd());
				res.showForWhomWasMadeReservation();
				
				reservationsList.add(res);
			}else{
				System.out.println("\nWe are sorry! There are no empty rooms with " + occup.getNrBeds() + " bed(s) "
						+ "for client: " + ((c!=null) ? c.getCNP() : null));
				System.out.println("During the period:");
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				System.out.println(sdf.format(occup.getBeg()) + " and " + sdf.format(occup.getEnd()));
			}
		}
	}
	
	public void addReservationToList(LinkedList<Client> cl, RoomList rList){
		Reservation newRes = null;
		Client client = ClientsList.addClientToList(cl);
		
		System.out.println("During which period you wish to make reservation?\n");
		AccomodationInterval interval = DateTimeUtil.enterAccomodationInterval();
		
		System.out.println("What conditions do you preffer?");
		Integer[] s = Room.specifyConditionsForYourRoom(); 
		
		Room room = Reservation.availableRoomWithSpecialConditions(ho, s, rList, 
													interval.getCheckInDate(), interval.getCheckOutDate());
		if(room!=null){		
			newRes = new Reservation(room,client,interval.getCheckInDate(),interval.getCheckOutDate());
			newRes.showForWhomWasMadeReservation();
			reservationsList.add(newRes);
		}else{
			System.out.println("\nWe are sorry! There are no empty rooms with your desired conditions");
			System.out.println("During the period:");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			System.out.println(sdf.format(interval.getCheckInDate()) + " and " + sdf.format(interval.getCheckOutDate()));

		}
	}
	
	public LinkedList<Reservation> getReservationsList(){
		return reservationsList;
	}
}
