package clientsPackage;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Client {
	private String CNP;				
	private int fidelity;			
	private int age;
	private Date birthday;
	private String specialSituations;

	public int calculateAge(){
		GregorianCalendar gDBirth = new GregorianCalendar();
		gDBirth.setTime(birthday);
		GregorianCalendar currentDate= new GregorianCalendar();
		currentDate.setTime(new Date());
		return currentDate.get(GregorianCalendar.YEAR)-gDBirth.get(GregorianCalendar.YEAR);
	}
	public static Client dataForNewClient(){
		Client cl = new Client();
		Scanner input = new Scanner(System.in);
		System.out.println("Enter CNP: ");
		cl.setCNP(input.nextLine());
		System.out.println("Enter client's fidelity (1-yes; 0-no): ");
		cl.setFidelity(input.nextInt());
		
		System.out.println("Enter client's birthday (dd-mm-yyyy): ");
		try{
			input.nextLine(); 		// cleaning the buffer
			String birthday = input.nextLine();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Date birthdayDate = df.parse(birthday);
			cl.setBirthday(birthdayDate);
		}catch(ParseException e){
			e.printStackTrace();
		}
		cl.calculateAge();
		
		System.out.println("Enter special situation (if there is any):");
		String specialSituation = input.nextLine();
		if(specialSituation==""){
			specialSituation="no special situation";
		}
		cl.setSpecialSituations(specialSituation);
		
		return cl;
	}
//sets&gets
	public void setAge(int age){
		this.age=age;
	}
	public int getAge(){
		return age;
	}
	public void setCNP(String CNP){
		this.CNP=CNP;
	}
	
	public String getCNP(){
		return CNP;
	}
	
	public void setFidelity(int fidelity){
		this.fidelity=fidelity;
	}
	
	public int getFidelity(){
		return fidelity;
	}
	
	public void setSpecialSituations(String ss){
		this.specialSituations=ss;
	}
	
	public String getSpecialSituations(){
		return this.specialSituations;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
}



