package clientsPackage;

import java.util.LinkedList;

import utilitary.FileRead;

public class ClientsList {
	private LinkedList<Client> clientsList;
	
	public void initializeClientsList(){
		FileRead fr = new FileRead();
		fr.readingFromFileClientsInfo();
		clientsList = fr.getClientsList();
	}
	
	public static Client addClientToList(LinkedList<Client> clientList){
		Client cl = Client.dataForNewClient();
		int promotieFidelitate = 0; // daca promotieFidelitate==3 atunci clientul va deveni fidel
		for(Client client: clientList){
			if( cl.getCNP().equals(client.getCNP())  ){
				promotieFidelitate++;
				if(promotieFidelitate==3){
					cl.setFidelity(1);
					break;
				}
			}
		}
		clientList.add(cl);
		return cl;
	}
	
	public LinkedList<Client> getClientsList(){
		return clientsList;
	}
}
