package roomPackage;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

import utilitary.DateTimeUtil;

public class Room {

	private int roomNumber;
	private int floor;
	private Status s;
	private int nrBeds;
	private int price;
	private LinkedList<RoomConditions> r;

	public Room(int roomNumber, int floor, int nrBeds, int price, LinkedList<RoomConditions> r) {
		this.roomNumber = roomNumber;
		this.floor = floor;
		this.s = Status.FREE;
		this.nrBeds = nrBeds;
		this.price = price;
		this.r = r;
	}
	
	public void determineRoomStatus(HashMap<Date,Date> occupationDates){
		Date currentDate = new Date();
		DateTimeUtil dtu = new DateTimeUtil();
		
		for(Date beg : occupationDates.keySet()){
			Date end = occupationDates.get(beg);
			if(dtu.isDateInInterval(currentDate,beg,end)){
				this.s = Status.OCCUPIED;
				break;
			}
			this.s = Status.FREE;
		}
		if(this.s!=Status.OCCUPIED){
			for(Date beg : occupationDates.keySet()){
				if(dtu.compare(currentDate, beg)==-1){
					this.s = Status.RESERVED;
					break;
				}else{
					this.s = Status.FREE;
				}
			}
		}
	}

	public static Integer[] specifyConditionsForYourRoom(){
		Integer[] s = {-1,0,0,0,0,0,0,0};
		int i=0;	//floor,nrBeds,balcony,TV,miniBar,Internet,kitchen,conditioning
		Scanner input = new Scanner(System.in);
		System.out.println("Floor (0 to 2, or -1 if doesn't matter): ");
		Integer floor = input.nextInt();
		s[i]=floor;
		i++;
		System.out.println("Nr of beds (1 to 4): ");
		Integer nrBeds = input.nextInt();
		s[i]=nrBeds;
		i++;
		System.out.println("Balcony (1-Yes; 0-if doesn't matter): ");
		Integer balcony = input.nextInt();
		s[i]=balcony;
		i++;
		System.out.println("TV (1-Yes; 0-if doesn't matter): ");
		Integer tv = input.nextInt();
		s[i]=tv;
	/*	i++;
		System.out.println("miniBar (1-Yes; 0-if doesn't matter): ");
		Integer miniBar = input.nextInt();
		s[i]=miniBar;
		i++;
		System.out.println("internet (1-Yes; 0-if doesn't matter): ");
		Integer internet = input.nextInt();
		s[i]=internet;
		i++;
		System.out.println("kitchen (1-Yes; 0-if doesn't matter): ");
		Integer kitchen = input.nextInt();
		s[i]=kitchen;
		i++;
		System.out.println("conditioning (1-Yes; 0-if doesn't matter): ");
		Integer conditioning = input.nextInt();
		s[i]=conditioning;*/

		return s;
	}
//sets&gets
	public void setStatus(Status s){
		this.s=s;
	}
	public Status getStatus(){
		return s;
	}
	public void setPrice(int price){
		this.price=price;
	}
	public int getPrice(){
		return price;
	}
	public void setRoomNumber(int rn){
		this.roomNumber=rn;
	}
	public int getRoomNumber(){
		return roomNumber;
	}
	public void setNrBeds(int nrBeds){
		this.nrBeds=nrBeds;
	}
	public int getNrBeds(){
		return nrBeds;
	}
	public void setFloor(int floor){
		this.floor=floor;
	}
	public int getFloor(){
		return floor;
	}
	public void setRoomConditions(LinkedList<RoomConditions> rc){
		this.r=rc;
	}
	public LinkedList<RoomConditions> getRoomConditions(){
		return r;
	}
}



