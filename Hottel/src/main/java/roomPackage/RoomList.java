package roomPackage;

import java.util.LinkedList;

import billsAndReservationPackage.HotelOccupation;
import utilitary.FileRead;

public class RoomList {
	private LinkedList<Room> allRoomsList;
	private HotelOccupation ho;
	
	public void initializeRoomList(){
		FileRead fr = new FileRead();
		fr.readFromFileRoomsInformation();
		allRoomsList = fr.getRoomsList();
	}
	
	public LinkedList<Room> getRoomsWithNrOfBeds(int nrBeds){
		LinkedList<Room> roomsWithXBeds = new LinkedList<Room>();
		for( Room r : allRoomsList){
			if( r.getNrBeds() == nrBeds ){
				roomsWithXBeds.add(r);
			}
		}
		return roomsWithXBeds;
	}
//////////////////////////////////////////////////
	public LinkedList<Room> getRoomsByFloor(int floor, LinkedList<Room> fromList){
		LinkedList<Room> roomsByFloor = new LinkedList<Room>();
		for( Room r : fromList){
			if( r.getFloor() == floor ){
				roomsByFloor.add(r);
			}
		}
		return roomsByFloor;
	}
	
	public LinkedList<Room> getRoomsWithBalcony(LinkedList<Room> fromList){
		LinkedList<Room> roomsWithBalcony = new LinkedList<Room>();
		for( Room r : fromList){
			LinkedList<RoomConditions> rc = r.getRoomConditions();
			for(RoomConditions condition : rc){
				if(condition == RoomConditions.balcony ){
					roomsWithBalcony.add(r);
				}
			}
		}
		return roomsWithBalcony;
	}
	
	public LinkedList<Room> getRoomsWithTV(LinkedList<Room> fromList){
		LinkedList<Room> roomsWithTV = new LinkedList<Room>();
		for( Room r : fromList){
			LinkedList<RoomConditions> rc = r.getRoomConditions();
			for(RoomConditions condition : rc){
				if(condition == RoomConditions.TV ){
					roomsWithTV.add(r);
				}
			}
		}
		return roomsWithTV;
	}
	
	public LinkedList<Room> getRoomsWithSpecialConditions(Integer[] specialCond){
		//specialCond = {floor,nrBeds,balcony,TV,miniBar,Internet,kitchen,conditioning}
		LinkedList<Room> roomsWithDemandedConditions = getRoomsWithNrOfBeds(specialCond[1]);
		if(specialCond[0]!=-1){
			roomsWithDemandedConditions = getRoomsByFloor(specialCond[0],roomsWithDemandedConditions);
		}
		if(specialCond[2]!=0){
			roomsWithDemandedConditions = getRoomsWithBalcony(roomsWithDemandedConditions);
		}
		if(specialCond[3]!=0){
			roomsWithDemandedConditions = getRoomsWithTV(roomsWithDemandedConditions);
		}
		/*
		if(specialCond[4]!=0){
			
		}
		if(specialCond[5]!=0){
			
		}
		if(specialCond[6]!=0){
			
		}
		if(specialCond[7]!=0){
			
		}*/
		return roomsWithDemandedConditions; 
	}
	
	public LinkedList<Room> getRoomsList(){
		return allRoomsList;
	}
}
