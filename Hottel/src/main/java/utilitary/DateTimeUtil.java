package utilitary;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DateTimeUtil {
	
	public int compare(Date d1, Date d2){
		if(d1.before(d2)){
			return -1;
		}else{
			if(d1.equals(d2)){
				return 0;
			}else{
				return 1;
			}
		}
	}
	
	public boolean isDateInInterval(Date d, Date x, Date y){
		if(compare(d,x)==-1 || compare(d,y)==1){
			return false;
		}else{
			return true;
		}
	}
	
	public boolean intervalOverlap(Date d1, Date d2, Date x, Date y){
		if(isDateInInterval(d1,x,y) || isDateInInterval(d2,x,y) || compare(d1,x)==0 || compare(d2,y)==0){
			return true;
		}else{
			return false;
		}
	}

	public static AccomodationInterval enterAccomodationInterval(){
		AccomodationInterval interval;
		Scanner input = new Scanner(System.in);
		Date checkInDate = new Date();
		Date checkOutDate = new Date();
		try{
			System.out.println("Check in date (dd-mm-yyyy): ");
			String checkIn = input.nextLine();
			System.out.println("Check out date (dd-mm-yyyy): ");
			String checkOut = input.nextLine();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			checkInDate = df.parse(checkIn);
			checkOutDate = df.parse(checkOut);
			interval = new AccomodationInterval(checkInDate,checkOutDate);
			return interval;
		}catch(ParseException e){
			e.printStackTrace();
		}
		return null;
	}
}


