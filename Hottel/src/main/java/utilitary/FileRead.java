package utilitary;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;

import billsAndReservationPackage.OccupationRequest;
import clientsPackage.Client;
import roomPackage.Room;
import roomPackage.RoomConditions;

public class FileRead {
	private LinkedList<OccupationRequest> occupationList;
	//////////////////////////////////////////////
	private final int totalClients = 5;
	private LinkedList<Client> clientsList;
	///////////////////////////////////////
	private final int totalRooms = 3;
	private LinkedList<Room> roomsList;

	
	public void readingOccupation(){
		occupationList = new LinkedList<OccupationRequest>();
		try{
			Scanner x = new Scanner(new File("Occupation.txt"));
			while(x.hasNextLine()){
				String checkIn = x.nextLine();
				String checkOut = x.nextLine();
				String CNP = x.nextLine();
				int nrBeds = Integer.parseInt(x.nextLine());
				try{
					SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					Date beg = df.parse(checkIn);
					Date end = df.parse(checkOut);
					OccupationRequest occ = new OccupationRequest(CNP, nrBeds, beg, end);
					occupationList.add(occ);
				}catch(ParseException e){
					e.printStackTrace();
				}
			}
			closeFile(x);
		}catch(Exception e){
			e.printStackTrace();
		} 
	}
	
	public void readingFromFileClientsInfo(){
		clientsList = new LinkedList<Client>();
		for(int i = 1; i <= totalClients; i++)
		{
			try{
				Scanner x = new Scanner(new File("checkClient"+i+".txt"));
				clientsList.add(readingClientInfo(x));
				closeFile(x);
			}catch(Exception e){
				e.printStackTrace();
			}  	
		}
	}

	public Client readingClientInfo(Scanner x){
		Client cl = new Client();
		cl.setCNP(x.nextLine());
		cl.setFidelity(Integer.parseInt(x.nextLine())); 
		try{
			String bd = x.nextLine();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Date dateOfBirth = df.parse(bd);
			cl.setBirthday(dateOfBirth);
		}catch(ParseException e){
			e.printStackTrace();
		}
		cl.setSpecialSituations(x.nextLine());
		return cl;
	}
//////////////////////////////////////////////////////////////////////////////////////////////
	public void readFromFileRoomsInformation(){
		roomsList = new LinkedList<Room>();
		for(int i=1; i<=totalRooms; i++){
			try{
				Scanner x = new Scanner(new File("Room"+i+".txt"));
				roomsList.add(readingRoomInfo(x));
				closeFile(x);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public Room readingRoomInfo(Scanner x){
		int roomNumber;
		int floor;
		int nrBeds;
		int pricePerNight;
		LinkedList<RoomConditions> rc = new LinkedList<RoomConditions>();
		
		roomNumber = Integer.parseInt(x.nextLine());
		floor = Integer.parseInt(x.nextLine());
		nrBeds = Integer.parseInt(x.nextLine());
		pricePerNight = Integer.parseInt(x.nextLine());
		while(x.hasNextLine()){
			String next = x.nextLine();
			if(next.equals("balcony")){
				rc.add(RoomConditions.balcony); 
			}
			if(next.equals("miniBar")){
				rc.add(RoomConditions.miniBar); 
			}
			if(next.equals("kitchen")){
				rc.add(RoomConditions.kitchen); 
			}
			if(next.equals("TV")){
				rc.add(RoomConditions.TV); 
			}
			if(next.equals("Internet")){
				rc.add(RoomConditions.Internet); 
			}
			if(next.equals("conditioning")){
				rc.add(RoomConditions.conditioning); 
			} 
		}
		Room r = new Room(roomNumber,floor,nrBeds,pricePerNight,rc);
		return r;
	}
	
	
	private void closeFile(Scanner x){
		x.close();
	}

	public LinkedList<Client> getClientsList() {
		return clientsList;
	}
	
	public LinkedList<Room> getRoomsList() {
		return roomsList;
	}
	
	public LinkedList<OccupationRequest> getOccupationList(){
		return occupationList;
	}
	
}
